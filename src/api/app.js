
const express = require('express'),
      app = express()

module.exports = (config, logger) => {
   

    if(config.api.headers.disableXPoweredBy){
        app.disable('x-powered-by');    
    }
    
    app.set('trust proxy', config.api.headers.trustProxy);

    require('./middleware')(app, config.api, logger)

    require('./routes')(app, config.api, logger)

    return { app };
};