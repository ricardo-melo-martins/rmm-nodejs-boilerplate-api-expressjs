const cors = require("cors");

module.exports = (app, config, logger) => {

    app.use(cors(config));
    
    return app
}