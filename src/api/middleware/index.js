

module.exports = (app, config, logger) => {
  
  // logs
  require("./logs")(app, config, logger);

  // cors
  require("./cors")(app, config.cors, logger);


  return app;
};
