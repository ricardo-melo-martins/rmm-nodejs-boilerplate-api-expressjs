const morgan = require("morgan")

module.exports = (app, config, logger) => {
    
    let morganLogger = require("../../infrastructure/handlers/morganLogger")(logger);

    app.use(
        morgan("combined", {
          skip: function (req, res) {
            return res.statusCode < 400;
          },
          stream: morganLogger.stream,
        })
      );
    
    return app
}