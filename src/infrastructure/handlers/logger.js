(function () {
  "use strict";

  const { createLogger, format, transports } = require("winston");

  const infoFilter = format((info) => (info.level === "info" ? info : false));

  const warnFilter = format((info) => (info.level === "warn" ? info : false));

  const debugFilter = format((info) => (info.level === "debug" ? info : false));

  module.exports = (config, label) => {

    config.logs.file.info.format = format.combine(infoFilter());
    config.logs.file.warn.format = format.combine(warnFilter());
    config.logs.file.debug.format = format.combine(debugFilter(), format.prettyPrint());

    config.logs.label = (typeof label !=='undefined') ? label : config.logs.label

    config.logs.console = {
      format: format.combine(
        format.label({
          label: config.logs.label,
        }),
        format.timestamp({
          format: config.logs.format.timestamp,
        }),
        format.simple(),
        format.colorize(),
        format.printf(
          (info) =>
            `${info.level} : ${info.label} ${[info.timestamp]}: ${info.message}`
        )
      ),
    };

    let _logger = createLogger({
      format: format.combine(
        format.errors({ stack: true }),
        format.timestamp({
          format: config.logs.format.timestamp,
        }),
        format.json()
      ),
    });

    if (config.logs.enabled) {
      _logger.add(new transports.File(config.logs.file.error));
      _logger.add(new transports.File(config.logs.file.info));
      _logger.add(new transports.File(config.logs.file.warn));
    }

    if (!config.production) {
      _logger.add(new transports.File(config.logs.file.debug));
      _logger.add(new transports.Console(config.logs.console));
    }

    return _logger;
  };
})();
