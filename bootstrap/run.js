const paths = require('./paths'),
      config = require('../config/environments')(paths),
      requisites = require('./requirements')(config),
      logger = require(`${paths.PATH_APPS}/infrastructure/handlers/logger`)(config,'RUN')

const { app } = require(`${paths.PATH_APPS}/api/app`)(config, logger)

const apiStarted = () => logger.info(`Running on port ${config.api.server.port}.`)

app.listen(config.api.server.port, apiStarted)