const version = process.version.split("."),
  major = parseInt(version[0].replace(/\D/g, ""), 10);
minor = parseInt(version[1].replace(/\D/g, ""), 10);

module.exports = (config) => {
  if (
    major < config.requisites.node.major ||
    (major === config.requisites.node.major &&
      minor < config.requisites.node.minor)
  ) {
    console.info("\x1b[41m%s\x1b[0m", "REQUIREMENTS fail");

    console.warn("INCOMPATIBLE NODE VERSION");
    console.warn("⚠️  You are running Node " + version);
    console.warn("Please install a compatible Node version and try again");
  } else {
    console.info("\x1b[44m%s\x1b[0m", "REQUIREMENTS passed");
  }
};
