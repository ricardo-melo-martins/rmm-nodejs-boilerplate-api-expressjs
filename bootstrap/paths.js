const appRoot = require('app-root-path'),
     {resolve} = require('path')

/*
 * The full path to the directory which holds "src", WITHOUT a trailing DS.
 */
let PATH_ROOT = appRoot

/*
 * The actual directory name for the application directory. Normally
 * named 'src'.
 */
let APP_DIR = 'src'

/*
 * Path to the application's directory.
 */
let PATH_APPS = resolve(`${PATH_ROOT}`, APP_DIR) 

/*
 * Path to the config directory.
 */
let PATH_CONFIG = resolve(`${PATH_ROOT}`, 'config')

/*
 * Path to the temporary files directory.
 */
let PATH_TMP = resolve(`${PATH_ROOT}`, 'tmp')

/*
 * Path to the logs directory.
 */
let PATH_LOGS = resolve(`${PATH_TMP}`, 'logs')

/*
 * Path to the cache files directory. It can be shared between hosts in a multi-server setup.
 */
let PATH_CACHE = resolve(`${PATH_TMP}`, 'cache')

module.exports = {
    PATH_ROOT,
    PATH_LOGS,
    PATH_APPS,
    PATH_CONFIG,
    PATH_TMP,
    PATH_LOGS,
    PATH_CACHE
}
      
      
