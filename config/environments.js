
module.exports = (paths) => {

  let PATH_ROOT = paths.PATH_ROOT
  let PATH_LOGS = paths.PATH_LOGS
  let PATH_APPS = paths.PATH_APPS

  let config = {
    path_root: PATH_ROOT,
    production: process.env.NODE_ENV === 'production' || false,
    api: {
      baseEndpoint: '/api',
      server: {
        port: process.env.PORT || 3000
      },
      cors: {
        origin: '*',
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
        optionsSuccessStatus: 204
      },
      headers:{
        disableXPoweredBy: true,
        trustProxy: true
      }
    },
    logs: {
      enabled: true,
      path: PATH_LOGS,
      label: '',
      format:{
        timestamp: "DD-MM-YYYY HH:mm:ss",
      },
      console: {},
      file: {
          error: {
              filename: `${PATH_LOGS}/error.log`, 
              level: 'error',
              maxFiles: 10,
              maxsize: 1048576,
              format: false
          },
          info: {
              filename: `${PATH_LOGS}/info.log`, 
              level: 'info',
              maxFiles: 10,
              maxsize: 1048576,
              format: false
          }, 
          warn: {
              filename: `${PATH_LOGS}/warn.log`, 
              level: 'warn',
              maxFiles: 10,
              maxsize: 1048576,
              format: false
          }, 
          debug: {
              filename: `${PATH_LOGS}/debug.log`, 
              level: 'debug',
              maxFiles: 1,
              maxsize: 1048576,
              format: false
          }
      },
    },
    requisites:{
      node: {
        major: 12,
        minor: 9
      }
    }
  }
  return config
}
